This is a helper mod designed to make it easier to implement custom modded secret shops.

It provides three major helper functions to do this.

`addDemoRoom(mod, name, items, texture, runeComponents)`  creates a secret room in 1-1 with an entrance rune right in the 1-1 starting room. Items is a list of strings of item names, and these items will be available in the room. This is designed especially for distributing modded items in an easy way.
Example: `addDemoRoom('DemoRoomDemo', 'IMBA', {'FeetBootsLunging','ShovelCourage','RingWonder'}, "mods/DemoRoomDemo/gfx/myRuneSprite.png")`, would spawn a normalish looking secret shop with Lunging, a Courage Shovel, and Ring of Wonder.

`addSecretShop(mod, name, width, height, texture, wallTier, runeComponents)` attemps to create a new secret shop that mimics the spawning patterns of vanilla secret shops. It will not spawn on the same floor as any other secret shop.

`addCustomShop(mod, name, width, height, texture, whichFloors, whichTile, wallTier, runeComponents)` is the most customizable way to add a secret room. `whichFloors` is a function which is called on each floor to check if the secret room will be generated on that floor, and 'whichTile' is a function that returns the tile the rune is to be generated on.

All 3 of these functions return an integer ID.
To customize the secret shops further one can call `CustomSecretShop.getBounds(id)' to get the bounds and then modify the interior.
The shop is generated as a handler to the `levelLoad` event with order `levelBounds` and sequence number 1, so be sure to make modifications after this.


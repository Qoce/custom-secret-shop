--Helper mod for adding custom secret shops

local event = require "necro.event.Event"
local collision = require "necro.game.tile.Collision"
local attack = require "necro.game.character.Attack"
local team = require "necro.game.character.Team"
local object = require "necro.game.object.Object"
local currentLevel = require "necro.game.level.CurrentLevel"
local components = require "necro.game.data.Components"
local customEntities = require "necro.game.data.CustomEntities"
local minimapTheme = require "necro.game.data.tile.MinimapTheme"
local utils = require "system.utils.Utilities"
local tile = require "necro.game.tile.Tile"
local rng = require "necro.game.system.RNG"
local tileTypes = require "necro.game.tile.TileTypes"
local snapshot = require "necro.game.system.Snapshot"

--unique rng channel for this mod specifically
--used for picking the to place the entrance rune on in normalCrackedWall
--regular SECRET_SHOP channel used otherwise
local rngChannel = 24601

local CustomSecretShop = {}

--Constant used to reset the secret shops array every time a new run happens
SS = snapshot.variable()
SS = {}

SecretShops = snapshot.runVariable(nil)
StartRoomCount = snapshot.levelVariable(0)
EmptyFloors = snapshot.runVariable(7)
ExclusiveShopsLeft = snapshot.runVariable(nil)

components.register{
    customRune = {}
}

function CustomSecretShop.isFirstFloor(shop, level)
    return currentLevel.getFloor() == 1 and currentLevel.getDepth() == 1
end

function CustomSecretShop.isAnyNonBossFloor(shop, level)
    return currentLevel.getFloor() < 4
end

--Picks a wall to place the rune under
--Attempts to mimic how secret shops naturally pick walls
--Is not perfect, for example it could spawn a secret shop in the interior of a secret room
local function normalCrackedWall(level, wallTier)
    local xstart = level.segments[1].bounds[1]
	local ystart = level.segments[1].bounds[2]
	local width = level.segments[1].bounds[3]
	local height = level.segments[1].bounds[4]

	local x,y
	local numAttempts = 0
	--Makes 200 attempts to place the wall
	while numAttempts < 200 do
		x = xstart + rng.int(width, rngChannel)
		y = ystart + rng.int(height, rngChannel)
        local info = tile.getInfo(x,y)
        if info.digResistance and ((wallTier == 3 and info.digResistance < 4) or (info.digResistance == wallTier) or info.digResistance == 0) then
            if info.name ~= info.cracked then
                local coords = {{x+1,y},{x-1,y},{x,y+1},{x,y-1}}
                for i = 1,4 do
                    if not tile.isSolid(coords[i][1], coords[i][2]) then
                        return {
                            x = x,
                            y = y,
                        }
                    end
                end
            end
        end
		numAttempts = numAttempts + 1
	end
    return {
        x = -3,
        y = -3
    }
end

function CustomSecretShop.normalCrackedWall(wallTier)
    return function (shop) return normalCrackedWall(shop,wallTier) end
end

--Places the rune directly in the bottom left corner of the starting room with no wall above its
--Supports multiple runes
function CustomSecretShop.openTileInStartingRoom(shop, level)
    StartRoomCount = StartRoomCount + 1
    return {
        x = -3 + StartRoomCount,
        y = 2
    }
end

-- Used for exclusive secret shops
-- First calculates the probablility that a custom secret shop will generate on this floor which is
-- The number of custom secret shops left to be generated over the number of empty floors left
local function secretShopOnThisFloor()
    if rng.range(1,EmptyFloors, rng.Channel.SECRET_SHOP) <= #ExclusiveShopsLeft then
        return rng.range(1,#ExclusiveShopsLeft, rng.Channel.SECRET_SHOP)
    end
end

local function inverseIndex(values)
    local index = {}
    for k,v in pairs(values) do
        index[v]=k
    end
    return index
end

local function placeWallTorches(bounds, offset)
    local cx = bounds[1] + bounds[3] / 2 - 1
    local cy = bounds[2] + bounds[4] / 2 - 1
    if offset[1] > 0 then
        object.spawn("WallTorch", cx + offset[1], bounds[2] + 1)
        object.spawn("WallTorch", cx - offset[1], bounds[2] + 1)
        object.spawn("WallTorch", cx + offset[1], bounds[2] + bounds[4] - 2)
        object.spawn("WallTorch", cx - offset[1], bounds[2] + bounds[4] - 2)
    else
        object.spawn("WallTorch", bounds[1] + 1, cy + offset[2])
        object.spawn("WallTorch", bounds[1] + 1, cy - offset[2])
        object.spawn("WallTorch", bounds[1] + bounds[3] - 2, cy + offset[2])
        object.spawn("WallTorch", bounds[1] + bounds[3] - 2, cy - offset[2])
    end
end
--Places an individaul free item with displacement dx,dy from the rune.
local function placeItem(bounds, level, dx, dy, item)
    local cx = bounds[1] + bounds[3] / 2 - 1
    local cy = bounds[2] + bounds[4] / 2 - 1
    object.spawn(item, cx + dx, cy + dy)
end
--Places free items across the center of the shop, two tiles above the rune
local function placeItems(bounds, level, items)
    local start = math.floor(-#items / 2)
    for i = 1, #items do
        placeItem(bounds, level, start + i, 0, items[i])
    end
end

--Creates a new segment containing the outline of the secret room
--Width and height are the width and height of the open part of the room and do not include the level boundary or shop walls
--If freeItems is specified then it will place the items in the middle of the shop two tiles above the rune
local function makeSecretRoom(ev, width, height, freeItems)
    dbg("Secret Room Made")
    local aw = width + 4
    local ah = height + 4
    local newSeg = {
        bounds = {ev.segments[#ev.segments].bounds[1] - 3 - aw, ev.segments[#ev.segments].bounds[2] - 3 - ah, aw, ah},
        tiles = {}
    }
    local nameMap = inverseIndex(ev.tileMapping.tileNames)
    for i = 1, aw * ah do
        if i <= aw or i > aw * ah - aw or i % aw <= 1 then
            newSeg.tiles[i] = nameMap["LevelBorder"]
        elseif i <= 2 * aw or i > aw * ah - 2 * aw or i % aw == 2 or i % aw == aw - 1 then
            newSeg.tiles[i] = nameMap["ShopWall"]
        else
            newSeg.tiles[i] = nameMap["Floor"]
        end
    end

    placeWallTorches(newSeg.bounds, {2, 0})
    placeWallTorches(newSeg.bounds, {0, 2})
    ev.segments[#ev.segments + 1] = newSeg
    if freeItems then
        placeItems(newSeg.bounds, ev, freeItems)
    end
    return newSeg.bounds
end

local wallNames = {"DirtWall", "StoneWall", "CatacombWall"}

local function placeRunes(ev, bounds, whichTile, name, wallTier)
    local r1 = object.spawn(name, bounds[1] + bounds[3] / 2 - 1, bounds[2] + bounds[4] - 4)
    local exitLoc = whichTile(ev)
    local r2 = object.spawn(name, exitLoc.x,exitLoc.y)
    r1.trapTravel.x = r2.position.x
    r1.trapTravel.y = r2.position.y
    r2.trapTravel.x = r1.position.x
    r2.trapTravel.y = r1.position.y
    r1.trap.armed = true
    r2.trap.armed = true
    if tile.isSolid(exitLoc.x, exitLoc.y) then
        local info = tile.getInfo(exitLoc.x,exitLoc.y)
        local zoneID = tileTypes.lookUpZoneID(info.tileset)
        local tileID = tileTypes.lookUpTileID(wallNames[wallTier], zoneID)
        tile.set(exitLoc.x, exitLoc.y, tileTypes.getCrackedTileID(tileID))
    end
end
--okay so let's pretend this didn't end up becomeing extremely heretical
event.levelLoad.add("spawnCustomSecretShops", {order = "levelBounds", sequence = 1}, function(ev)
    if not SecretShops then
        SecretShops = utils.deepCopy(SS)
        ExclusiveShopsLeft = {}
        for i = 1,#SecretShops do
            if SecretShops[i].exclusive then
                ExclusiveShopsLeft[#ExclusiveShopsLeft + 1] = SecretShops[i]
                ExclusiveShopsLeft[#ExclusiveShopsLeft].index = i
            end
        end
    end
    local shopChoice
    if currentLevel.getFloor() < 4 and #ev.segments < 2 then
        shopChoice = secretShopOnThisFloor()
        EmptyFloors = EmptyFloors - 1
    end
    for i = 1,#SecretShops do
        SecretShops[i].justGenerated = false --this variable is named incorrectly and actually refers to specifically exclusive shops that generated this floor, it will be false for non-exclusive shops
    end
    if shopChoice and shopChoice > 0 then
        SecretShops[ExclusiveShopsLeft[shopChoice].index].bounds = makeSecretRoom(ev, ExclusiveShopsLeft[shopChoice].width, ExclusiveShopsLeft[shopChoice].height, ExclusiveShopsLeft[shopChoice].items)
        SecretShops[ExclusiveShopsLeft[shopChoice].index].generated = true
        SecretShops[ExclusiveShopsLeft[shopChoice].index].justGenerated = true
        table.remove(ExclusiveShopsLeft,shopChoice)
    end
    for i = 1, #SecretShops do
        if SecretShops[i].whichFloors(SecretShops[i],ev) then
            SecretShops[i].bounds = makeSecretRoom(ev, SecretShops[i].width, SecretShops[i].height, SecretShops[i].items)
            SecretShops[i].generated = true
        elseif not SecretShops[i].justGenerated then
            SecretShops[i].bounds = nil
        end
    end
end)

event.levelLoad.add("spawnCustomSecretShopRunes", {order = "entities", sequence = 1}, function (ev)
    for i = 1, #SecretShops do
        if SecretShops[i].bounds then
            placeRunes(ev, SecretShops[i].bounds, SecretShops[i].whichTile, SecretShops[i].name, SecretShops[i].wallTier)
        end
    end
end)


local function makeCustomTravelRune(params, customComponents)
    customEntities.register(utils.mergeTablesRecursive({
        name = params.name,
		position = {},
		health = {},
		collision = { mask = collision.Type.TRAP },
		trap = { targetFlags = attack.Flag.DEFAULT },
        visibleOnWallRemoval = {},
        trapArmOnVictimMove = {},
		silhouette = {},
		spriteSheet = {frameX = 2},
		spriteDecal = { z = 1 },
		minimapStaticPixel = {
			color = minimapTheme.Color.TRAP,
			depth = minimapTheme.Depth.TRAP,
		},
        trapTravel = {},
        trapXMLMapping = { id = 8 },
        trapSubtypeXMLMapping = { id = 0 },
        team = { id = team.Id.ENEMY },
        attackable = false,
		sprite = { texture = params.texture },
        secretFightMarker = {type = 7},
		positionalSprite = {
			offsetX = -1,
			offsetY = 12,
		},
		soundTrapActivate = { sound = "teleport" },
		soundWallRemoval = { sound = "secretFound" },
		trapVocalize = { component = "voiceTeleport" },
        visibility = {},
        gameObject = {},
        CustomSecretShop_customRune = {},
    }, customComponents))
end

--Checks if a secret shop has been registered with the same name, returns the index of such a shop if one exists
local function checkName(mod, name)
    for i = 1,#SS do
        if SS[i].name == mod.."_"..name then
            return i
        end
    end
    return nil
end

--Adds a shop with a easily accessable rune in the 1-1 starting room that has the items available for free.
--Designed to be an easy way to hand out modded items for playtesting sessions
--Returns the ID of the shop
function CustomSecretShop.addDemoRoom(mod, name, items, texture, runeComponents)
    local n = #SS + 1
    if checkName(mod,name) then
        n = checkName(mod, name)
    end
    if not runeComponents then
        runeComponents = {}
    end
    SS[n] = {
        exclusive = false,
        items = items,
        generated = false, --true if this shop has EVER been generated, even if it wasn't this floor
        whichFloors = CustomSecretShop.isFirstFloor,
        whichTile = CustomSecretShop.openTileInStartingRoom,
        width = math.max(5, 2 + #items),
        height = 7,
        name = mod.."_"..name,
        wallTier = 2, --doesn't actually matter
    }
    makeCustomTravelRune({
        name = name,
        texture = texture,
    }, runeComponents)
    return n
end

--Adds a shop that attempts to behave similarly to normal secret shops
--These shops will not spawn on the same floor as any vanilla secret shop or any other secret shop with exclusive = true
--These shops spawn under a cracked wall of the specified tier given by wallTier
--Returns the ID of the shop
function CustomSecretShop.addSecretShop(mod, name, width, height, texture, wallTier, runeComponents)
    local n = #SS + 1
    if checkName(mod,name) then
        n = checkName(mod, name)
    end
    if not runeComponents then
        runeComponents = {}
    end
    SS[n] = {
        exclusive = true,
        generated = false,
        whichFloors = function (x) return false end,
        whichTile = CustomSecretShop.normalCrackedWall(wallTier),
        width = width,
        height = height,
        name = mod.."_"..name,
        wallTier = wallTier
    }
    makeCustomTravelRune({
        name = name,
        texture = texture,
    }, runeComponents)
    return n
end

--Adds a secret shop with customizable conditions to pick which floor(s) it spawns on
--Returns the index of the shop
function CustomSecretShop.addCustomShop(mod, name, width, height, texture, whichFloors, whichTile, wallTier, runeComponents)
    local n = #SS + 1
    if checkName(mod,name) then
        n = checkName(mod, name)
    end
    if not runeComponents then
        runeComponents = {}
    end
    SS[n] = {
        exclusive = false,
        generated = false,
        whichFloors = whichFloors,
        whichTile = whichTile,
        width = width,
        height = height,
        name = mod.."_"..name,
        wallTier = wallTier
    }
    makeCustomTravelRune({
        name = name,
        texture = texture,
    }, runeComponents)
    return n
end

--Given the index of a shop, returns the boundary of the shop
function CustomSecretShop.getBounds(id)
    return SecretShops[id].bounds
end

--Resets the secret shops
--Usefull to include at the top of your mod when developing it to avoid having to reload this mod as often when you reload your own mod.
function CustomSecretShop.reset()
    dbg("Warning: Reset function used on CustomSecretShop. This can interfere with any other mods that rely on CustomSecretShop.")
    SS = {}
end


return CustomSecretShop